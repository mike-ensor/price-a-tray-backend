#!/bin/bash

echo "========================================="
echo "========================================="
echo "Starting Migrations"
echo "Migrations Version ${INIT_VERSION}"
echo "========================================="
echo -e "=========================================\n"

# Allow an ENV to override the default of /app
export RUST_LOG="${RUST_LOG}:-debug"

MIGRATION_HOME="${MIGRATION_HOME:-$(pwd)}"
DB_HOST="${DB_HOST:-localhost}"
DB_PORT="${DB_PORT:-5432}"
DATABASE_URL="postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}"
# List of migrations to run
if [[ -z "${MIGRATION_LIST}" ]]; then
  MIGRATION_LIST=( "game-service" )
fi

FORCE_REDO_ALL="${FORCE_REDO_ALL:-false}"
ERROR=0

echo "========================================="
echo -e "DB_USER:\t${DB_USER}"
echo -e "DB_PASS:\t${DB_PASS}"
echo -e "DB_PORT:\t${DB_PORT}"
echo -e "DB_NAME:\t${DB_NAME}"
echo -e "FULL_URL:\t${DATABASE_URL}"
echo "========================================="
echo "Migration Folder(s):"
for item in "${MIGRATION_LIST[@]}"
do
  echo "${item}"
done
echo  -e "=========================================\n"

for item in "${MIGRATION_LIST[@]}"
do
    echo  -e "========================================="
    echo ":: Running migration [${item}]"
    echo "========================================="

    MIGRATION_PATH="${MIGRATION_HOME}/${item}"
    if [[ -d "${MIGRATION_PATH}" ]]; then

      # TODO: Use the --migration-dir flag instead of popd/pushd into directory
      # shellcheck disable=SC2164
      pushd "${MIGRATION_PATH}" > /dev/null

      # Setup ENV variables IF needed

      if [[ -f ".envrc" ]]; then
        source .envrc
      fi

      echo  -e "========================================="
      echo ":: List Migrations Folder"
      echo "========================================="
      ls -al migrations/
      echo "========================================="

      # Run migration
      if [[ ${FORCE_REDO_ALL} == true ]]; then
        echo ":::: Forcing Redo all migrations"
        diesel migration redo --all --database-url "${DATABASE_URL}"
      fi

      echo ":::: Running migrations"
      # fails if the schema is not locked. This is to prevent database from not being in sync with app version
      diesel migration run --locked-schema --database-url "${DATABASE_URL}"

      # Display any errors
      if [[ $? -gt 0 ]]; then
        echo ":: Error trying to migrate ${item}" 1>&2
        ERROR=1
      else
        echo -e "=========================================\n"
        diesel migration list --database-url "${DATABASE_URL}"
      fi

      # shellcheck disable=SC2164
      popd > /dev/null
    fi
done
if [[ $ERROR -gt 0 ]]; then
  echo "Errors found, please correct" 1>&2
  exit 1
fi