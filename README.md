# Overview

This project contains the Game services to support the Price a Tray game

## Services

Services are exposed using (REST or gRPC) and will provide core game functionality.

* GameManagement - Provides the lifecycle of the games. Create, delete, list, update a Game instance
* GameService - Calls and services supporting a single instance of a game
* UserService - Services related to users on the game system including participants and admins

## Deploying

This project relies on `ExternalSecret` [ExternalSecrets.io](httsp://external-secrets.io) for pulling the PostgreSQL 
database password. Furthermore, it uses `Google Secret Manger` as the secret store. Optionally, bypass this and create a
`Secret` (however) named `postgres-db-pass` in the cluster and remove the `ExternalSecret` from the manifests.

### Create PostgreSQL password

```shell
# Generate a password (this is not production ready, but works for demos)
export PASSWD="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c10)" 

echo -n "{\"password\":\"${PASSWD}\"}" | \
gcloud secrets create price-a-tray-postgres-password \
  --replication-policy="automatic" \
  --project="${PROJECT_ID}" \
   --data-file=-

echo "Your Password: ${PASSWD}"
unset $PASSWD
````

## Development

High-level, this is a Rust workspace application containing all of the above (and more) services. 

More details in the development guide.

### Environment Variables
```shell
export RUST_LOG="debug"
export DB_USER="some_user"
export DB_PASS="some_pass"
export DB_HOST="localhost"
export DB_NAME="price_a_tray"

export DATABASE_URL="postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}"

# Gitlab (pushing to registry.gitlab.com 
export GITLAB_PROJECT="mike-ensor/price-a-tray-backend"
export IMAGE_URL="registry.gitlab.com/${GITLAB_PROJECT}/${CONTAINER_NAME}"
export IMAGE_LATEST_FULL="${IMAGE_URL}:latest"
```


## Building & Pushing Containers

```shell
export VERSION="v0.0.1"
CONTAINER_NAME="price-a-tray-game" ./build.sh ${VERSION} ${CONTAINER_NAME} Dockerfile.game  
```

### Thanks & Cites

* https://github.com/dirien/quick-bites/blob/main/rust-actix-web-rest-api-opentelemetry/src/repository/database.rs
  * Big help in creating nested objects during results. The "belongs_to" only helps with the "in" clause of a SQL, but does not fill objects
  * Learning, need to create the objects that you intend on the user working with and fill with the data you need. The ORM will not do this for you

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg.git
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This project provides an API to inference extraced frames (image) against the edge model pulled from the GCS bucket.

