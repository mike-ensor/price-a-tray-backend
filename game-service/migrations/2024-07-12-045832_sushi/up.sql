INSERT INTO game_model (id, short_name, description, last_trained, source, active)
VALUES (7, 'SushiTown', 'Your friendly sushi neighborhood restaurant ', now(),
        'gs://price-a-tray-edge-models/sushitown/0.0.1', true);

INSERT INTO games (name, description, event_name, model_id, locale, active)
VALUES (e'Sushi Town',
        'Arrange your sushi platters to match the price before the time runs out',
        'Google NEXT Tokyo 2024',
        7,
        'ja-JP',
        false);

INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Red clam | 赤貝', 450.00, 650.00, 'akagai.jpg', 7, 'akagai'),
       ('Red Shrimp | 赤えび', 350.00, 550.00, 'akaebi.jpg', 7, 'akaebi'),
       ('Sweet shrimp | 甘えび', 625.00, 825.00, 'amaebi.jpg', 7, 'amaebi'),
       ('Corn-roll | コーン巻き', 230.00, 430.00, 'cornmaki.jpg', 7, 'cornmaki'),
       ('Surf Clam | ほっき貝', 300.00, 500.00, 'hokkigai.jpg', 7, 'hokkigai'),
       ('Scallop | 帆立', 440.00, 640.00, 'hotate.jpg', 7, 'hotate'),
       ('Salmon-roe | いくら', 430.00, 630.00, 'ikura.jpg', 7, 'ikura'),
       ('Sardine  | いわし', 300.00, 500.00, 'iwashi.jpg', 7, 'iwashi'),
       ('Cucumber-roll | かっぱ巻き', 200.00, 400.00, 'kappamaki.jpg', 7, 'kappamaki'),
       ('Herring roe | 数の子', 690.00, 890.00, 'kazunoko.jpg', 7, 'kazunoko'),
       ('Prawn | 車えび', 700.00, 900.00, 'kurumaebi.jpg', 7, 'kurumaebi'),
       ('Tuna | まぐろ', 550.00, 750.00, 'maguro.jpg', 7, 'maguro'),
       ('Salmon | サーモン', 465.00, 665.00, 'salmon.jpg', 7, 'salmon'),
       ('Tamago | 玉子', 215.00, 415.00, 'tamago.jpg', 7, 'tamago'),
       ('Tuna-roll | ツナ', 340.00, 540.00, 'tna.jpg', 7, 'tna'),
       ('eel | うなぎ', 190.00, 390.00, 'unagi.jpg', 7, 'unagi'),
       ('Urchin | うに', 810.00, 1010.00, 'uni.jpg', 7, 'uni');