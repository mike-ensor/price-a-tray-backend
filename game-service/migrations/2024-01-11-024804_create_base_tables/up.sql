-- Testing Data
-- ========================
-- BurgerTime Game ID: 8e1cced7-ea3d-4cae-98aa-6ee8fa210e3d


CREATE TABLE IF NOT EXISTS game_accelerator
(
    id          serial PRIMARY KEY  NOT NULL,
    name        VARCHAR(128) UNIQUE NOT NULL,
    description TEXT                NULL
);

INSERT INTO game_accelerator (name, description)
VALUES ('CPU', 'CPU'),
       ('TPU', 'Coral Edge TPU'),
       ('GPU', 'Nvidia GPU');

CREATE TABLE IF NOT EXISTS game_runtime
(
    id          serial PRIMARY KEY  NOT NULL,
    name        VARCHAR(128) UNIQUE NOT NULL,
    description TEXT                NULL
);

INSERT INTO game_runtime(name, description)
VALUES ('EDGE', 'Inference is performed at the edge'),
       ('CLOUD', 'Inference is performed in the cloud');

-- GameModel model
CREATE TABLE IF NOT EXISTS game_model
(
    id           serial PRIMARY KEY  NOT NULL,
    short_name   VARCHAR(128) UNIQUE NOT NULL,
    description  TEXT                NULL,
    last_trained TIMESTAMP           NOT NULL,
    source       VARCHAR(255)        NOT NULL,
    active       BOOLEAN             NOT NULL default true,
    created_at   TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP,
    updated_at   TIMESTAMP                    DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS game_model_object
(
    id          SERIAL PRIMARY KEY NOT NULL,
    name        VARCHAR(128)       NOT NULL,
    lower       FLOAT              NOT NULL,
    upper       FLOAT              NOT NULL,
    image_link  VARCHAR(255)       NULL,
    created_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    game_id     SERIAL             NOT NULL references game_model (id) ON DELETE CASCADE ON UPDATE CASCADE,
    model_label VARCHAR(64)        NOT NULL
);

CREATE TABLE IF NOT EXISTS game_status
(
    status      VARCHAR(64) PRIMARY KEY NOT NULL UNIQUE,
    description TEXT
);

-- Game model
CREATE TABLE IF NOT EXISTS games
(
    id           UUID PRIMARY KEY   NOT NULL DEFAULT uuid_generate_v4(),
    name         VARCHAR(64) UNIQUE NOT NULL,
    description  TEXT               NULL,
    event_name   VARCHAR(128)       NULL,
    active       BOOLEAN            NOT NULL DEFAULT false,
    model_id     INTEGER            NULL references game_model (id) ON DELETE CASCADE ON UPDATE CASCADE,
    game_status  VARCHAR(64)        NOT NULL DEFAULT 'NOT_STARTED' references game_status (status) ON DELETE SET NULL ON UPDATE SET NULL,
    created_at   TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at   TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
    locale       VARCHAR(10)        NOT NULL DEFAULT 'en-US',
    round_length INTEGER            NOT NULL DEFAULT 30,
    min_target   FLOAT              NOT NULL DEFAULT 8.00,
    max_target   FLOAT              NOT NULL DEFAULT 15.00
);

CREATE TABLE IF NOT EXISTS game_round_status
(
    id VARCHAR(64) PRIMARY KEY NOT NULL
);

INSERT INTO game_round_status
VALUES ('REGISTERED'),
       ('PLAYING'),
       ('FINISHED'),
       ('WITHDRAWN');

-- Creating a new Game Instance is like "signing up"
CREATE TABLE IF NOT EXISTS game_rounds
(
    id               UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
    player_id        UUID             NOT NULL,
    game_id          UUID             NOT NULL REFERENCES games (id) ON DELETE CASCADE ON UPDATE CASCADE,
    -- Game Settings
    status           VARCHAR(64)      NOT NULL REFERENCES game_round_status (id) ON DELETE CASCADE ON UPDATE CASCADE,
    target           float            NOT NULL,
    game_runtime     INTEGER          NOT NULL DEFAULT 1 REFERENCES game_runtime (id) ON DELETE SET NULL ON UPDATE CASCADE,
    game_accelerator INTEGER          NOT NULL DEFAULT 1 REFERENCES game_accelerator (id) ON DELETE SET NULL ON UPDATE CASCADE,
    -- Game Facts
    start_time       TIMESTAMP        NULL,
    end_time         TIMESTAMP        NULL,
    -- Object metadata
    created_at       TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Holds scores for a game instance
-- TODO: Add a link to an array of Game Items that made that score?
CREATE TABLE IF NOT EXISTS game_scores
(
    id            serial PRIMARY KEY NOT NULL,
    game_round_id UUID               NOT NULL REFERENCES game_rounds (id) ON DELETE CASCADE ON UPDATE CASCADE,
    score         float              NOT NULL,
    target        float              NOT NULL DEFAULT 0.0,
    initials      VARCHAR(50)        NULL,
    created_at    TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO game_status (status, description)
VALUES ('NOT_STARTED', 'The game has not yet started'),
       ('COMPLETED', 'The has ended'),
       ('IN_PROGRESS', 'The game is currently active and in-progress');

INSERT INTO game_model (id, short_name, description, last_trained, source, active)
VALUES (1, 'Burger Time', 'Set of objects related to a QSR', now(), 'gs://burger-time-models', true),
       (2, 'Best Box', 'Set of objects related to a big box store', now(), 'gs://best-box-models', true),
       (3, 'DIY', 'Set of objects related to a DIY home stores', now(), 'gs://diy-models', true),
       (4, 'Child Toys', 'Set of objects related to a children\s toys', now(), 'gs://child-toys-models', true),
       (5, 'Sea Store', 'Set of objects related to a convenience stores', now(), 'gs://sea-store-models', true);

INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Cheeseburger', 2.75, 5.75, 'http://burger-time/cheese-burger.jpg', 1, 'cheeseburger'),
       ('Filet o’ Fish', 4.05, 8.35, 'http://burger-time/fish.jpg', 1, 'filet_o_fish'),
       ('BigMac', 2.75, 5.75, 'http://burger-time/big-mac.jpg', 1, 'big_mac'),
       ('Quarter Pounder', 6.15, 9.25, 'http://burger-time/qp.jpg', 1, 'quarter_pounder_with_cheese'),
       ('Baked Apple Pie', 0.95, 1.35, 'http://burger-time/baked-apple.jpg', 1, 'apple_pie'),
       ('French Fries', 0.99, 2.65, 'http://burger-time/french-fries.jpg', 1, 'fries'),
       ('McNuggets (4 Pack)', 4.85, 6.15, 'http://burger-time/mcnuggets.jpg', 1, '4_piece_chicken_nugget'),
       ('Sausage Breakfast Wrap', 1.45, 2.75, 'http://burger-time/snack-wrap.jpg', 1, 'sausage_burrito'),
       ('Hashbrowns', 1.50, 3.00, 'http://burger-time/hashbrown.jpg', 1, 'hashbrown'),
       ('Honey Mustard Packet', 0.10, 0.50, 'http://burger-time/hot-mustard.jpg', 1, 'honey_mustard'),
       ('Spicy Sauce Packet', 0.25, 0.50, 'http://burger-time/sweet-sour.jpg', 1, 'spicy_sauce'),
       -- These are not images in the data set (yet)
       ('Hamburger', 2.25, 4.50, 'http://burger-time/hamburger.jpg', 1, 'hamburger'),
       ('Large Coke', 1.15, 4.05, 'http://burger-time/coke.jpg', 1, 'coke-large'),
       ('McChicken', 6.95, 9.05, 'http://burger-time/mc-chicken.jpg', 1, 'mcchicken'),
       ('McNuggets (20 Pack)', 11.35, 17.25, 'http://burger-time/mcnuggets.jpg', 1, 'mcnuggets-twenty-pack'),
       ('Pancake Breakfast', 12.75, 17.75, 'http://burger-time/pancake-breakfast.jpg', 1, 'pancake-breakfast'),
       ('Sausage Egg McMuffin', 3.25, 4.50, 'http://burger-time/sausage-egg-mcmuffin', 1, 'sausage-egg-mcmuffin'),
       ('Happy Meal', 6.00, 11.25, 'http://burger-time/happy-meal.jpg', 1, 'happy-meal'),
       ('Ketchup packets', 0.00, 0.25, 'http://burger-time/ketchup.jpg', 1, 'ketchup-packet');

INSERT INTO games (name, description, event_name, model_id, active)
VALUES ('SeaStore Game',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'Mountain Conference',
        4, false),
       ('DIY Home Game',
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
        'Cascade Conference',
        3, false);

-- Insert a game that has a "known" ID for testing
INSERT INTO games (id, name, description, event_name, model_id, locale, active)
VALUES ('8e1cced7-ea3d-4cae-98aa-6ee8fa210e3d',
        'Price a Tray',
        'Do your best to price the right items on your tray before the time runs out',
        E'McDonald\'s WWC 2024',
        1,
        'es-ES',
        false);
