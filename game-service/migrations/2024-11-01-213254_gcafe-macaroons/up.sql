-- Add Macaroons to dataset

INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Orange Macaroon', 0.10, 0.15, 'orange_mac.jpg', 8, 'orange_mac'),
       ('Strawberry Macaroon', 0.06, 0.17, 'strawberry_mac.jpg', 8, 'strawberry_mac'),
       ('Chocolate Macaroon', 0.14, 0.25, 'chocolate_mac.jpg', 8, 'chocolate_mac'),
       ('Blueberry Macaroon', 0.20, 0.31, 'blueberry_mac.jpg', 8, 'blueberry_mac'),
       ('Vanilla Macaroon', 0.25, 0.36, 'vanilla_mac.jpg', 8, 'vanilla_mac'),
       ('Lime Macaroon', 0.31, 0.42, 'lime_mac.jpg', 8, 'lime_mac'),
       ('Lemon Macaroon', 0.36, 0.47, 'lemon_mac.jpg', 8, 'lemon_mac'),
       ('Grape Macaroon', 0.42, 0.53, 'grape_mac.jpg', 8, 'grape_mac');
