DELETE
FROM game_model_object
WHERE game_id = 8
  AND (model_label = 'orange_mac'
    OR model_label = 'strawberry_mac'
    OR model_label = 'chocolate_mac'
    OR model_label = 'blueberry_mac'
    OR model_label = 'vanilla_mac'
    OR model_label = 'lime_mac'
    OR model_label = 'lemon_mac'
    OR model_label = 'grape_mac');
