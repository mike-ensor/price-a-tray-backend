UPDATE game_model_object
SET
    lower = 2.80,
    upper = 2.90
WHERE game_id = 8 AND
      model_label = 'fruit_punch_cup' OR
    model_label = 'double_strawberry_cup' OR
    model_label = 'oreo_fruit_cup';

UPDATE game_model_object
SET
    lower = 0.25,
    upper = 0.30
WHERE game_id = 8 AND
    model_label = 'green_apple_coffee';

UPDATE game_model_object
SET
    lower = 3.60,
    upper = 3.70
WHERE game_id = 8 AND
      model_label = 'sugar_cube_pastry' OR
    model_label = 'sweet_paradise_pastry';
