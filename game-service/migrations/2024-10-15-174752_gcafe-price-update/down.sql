UPDATE game_model_object
SET
    lower = 1.25,
    upper = 1.35
WHERE game_id = 8 AND
    model_label = 'green_apple_coffee';