INSERT INTO game_model (id, short_name, description, last_trained, source, active)
VALUES (8, 'Google Cafe', 'Google Cafe bakery items', now(),
        'gs://price-a-tray-edge-models/gcafe/0.0.5', true);

INSERT INTO games (name, description, event_name, model_id, locale, active)
VALUES (e'Bakery Blitz',
        'Can you get the right items before it is too late?',
        'Google Cloud Live 2024',
        8,
        'en-US',
        true);

INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Blue Kiwi Coffee', 1.50, 1.60, 'blue_kiwi_coffee.jpg', 8, 'blue_kiwi_coffee'),
       ('Chocolate Drizzle Mini', 4.75, 4.85, 'chocolate_drizzle_mini.jpg', 8, 'chocolate_drizzle_mini'),
       ('Chocolate Fruit Swiss', 1.45, 1.55, 'chocolate_fruit_swiss.jpg', 8, 'chocolate_fruit_swiss'),
       ('Chocolate Strawberry Coffee', 2.00, 2.10, 'chocolate_strawberry_coffee.jpg', 8, 'chocolate_strawberry_coffee'),
       ('Double Strawberry Cup', 2.80, 2.90, 'double_strawberry_cup.jpg', 8, 'double_strawberry_cup'),
       ('Fruit Punch Cup', 1.00, 2.00, 'fruit_punch_cup.jpg', 8, 'fruit_punch_cup'),
       ('Fruit Punch Swiss', 2.90, 3.00, 'fruit_punch_swiss.jpg', 8, 'fruit_punch_swiss'),
       ('Green Apple Coffee', 1.25, 1.35, 'green_apple_coffee.jpg', 8, 'green_apple_coffee'),
       ('Lime Swiss', 1.40, 1.50, 'lime_swiss.jpg', 8, 'lime_swiss'),
       ('Matcha Delight Mini', 4.25, 4.35, 'macha_delight_mini.jpg', 8, 'macha_delight_mini'),
--        ('Nuts Pastry', 1.00, 2.00, 'nuts_pastry.jpg', 8, 'nuts_pastry'),
       ('Orange Kiwi Coffee', 1.50, 1.60, 'orange_kiwi_coffee.jpg', 8, 'orange_kiwi_coffee'),
       ('Orange Swiss', 1.35, 1.45, 'orange_swiss.jpg', 8, 'orange_swiss'),
       ('Oreo Fruit Cup', 2.99, 3.09, 'oreo_fruit_cup.jpg', 8, 'oreo_fruit_cup'),
       ('Purple Kiwi Coffee', 1.75, 1.85, 'purple_kiwi_coffee.jpg', 8, 'purple_kiwi_coffee'),
       ('Red Apple Coffee', 0.75, 0.85, 'red_apple_coffee.jpg', 8, 'red_apple_coffee'),
       ('Strawberry Swiss', 1.25, 1.35, 'strawberry_swiss.jpg', 8, 'strawberry_swiss'),
       ('Strawberry Swirl Mini', 1.50, 1.60, 'strawberry_swirl_mini.jpg', 8, 'strawberry_swirl_mini'),
       ('Sugar Cube Pastry', 3.95, 4.00, 'sugar_cube_pastry.jpg', 8, 'sugar_cube_pastry'),
       ('Sugar Pastry', 3.60, 3.70, 'sugar_pastry.jpg', 8, 'sugar_pastry'),
       ('Sweet Paradise Pastry', 4.15, 4.25, 'sweet_paradise_pastry.jpg', 8, 'sweet_paradise_pastry'),
       ('Vanilla Swiss', 1.30, 1.40, 'vanila_swiss.jpg', 8, 'vanila_swiss');