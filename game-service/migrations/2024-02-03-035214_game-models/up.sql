INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Ball', 5.00, 6.50, 'http://child-toys/ball.jpg', 4, 'ball'),
       ('Foam Toy', 6.00, 11.25, 'http://child-toys/foam-toy.jpg', 4, 'foam-toy'),
       ('Fish', 6.00, 11.25, 'http://child-toys/fish.jpg', 4, 'fish'),
       ('Life Ring', 6.00, 11.25, 'http://child-toys/life-ring.jpg', 4, 'life-ring');


INSERT INTO game_model (id, short_name, description, last_trained, source, active)
VALUES (6, 'GTacos', 'Your friendly neighborhood taco stand', now(), 'gs://price-a-tray-edge-models/gtacos/0.0.2', true);

INSERT INTO games (id, name, description, event_name, model_id, active)
VALUES ('95f21337-2d02-4f21-8352-e61d57968df7',
        e'Google Tacos',
        'Do your best to price the right items on your tray before the time runs out',
        'Google NEXT 2024',
        6, false);

INSERT INTO game_model_object(name, lower, upper, image_link, game_id, model_label)
VALUES ('Cheese Enchilada', 5.20, 6.30, 'item.jpg', 6, 'cheese_enchilada'),
       ('Burger', 4.50, 6.00, 'item.jpg', 6, 'burger'),
       ('Wings', 3.75, 4.25, 'item.jpg', 6, 'wings'),
       ('Nuggets', 3.00, 4.00, 'item.jpg', 6, 'nuggies'),
       ('Pork Enchilada', 5.50, 6.75, 'item.jpg', 6, 'pork_enchilada'),
       ('French Fries', 2.00, 3.50, 'item.jpg', 6, 'fries'),
       ('Doughnuts', 1.20, 2.50, 'item.jpg', 6, 'doughnut'),
       ('Tacos', 2.90, 4.10, 'item.jpg', 6, 'tacos'),
       ('Corn', 1.50, 2.50, 'item.jpg', 6, 'corn'),
       ('Golden Hashbrowns', 0.50, 2.00, 'item.jpg', 6, 'hashbrown'),
       ('Steak Enchilada', 7.00, 11.0, 'item.jpg', 6, 'steak_enchilada');