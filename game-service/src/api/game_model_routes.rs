/*
NOTE: Decision made to not update Game Model or Game Model Objects for Version 1
*/

use actix_web::{get, HttpResponse, post, put, web};
use log::debug;
use serde_derive::Deserialize;

use web_utils::error_handler::CustomError;

use crate::models::game_model::{GameModelDAO, GameModelObject};
use crate::repository::game_db::Database;

#[derive(Debug, Deserialize)]
pub struct GameModelRequest {
    active: bool,
}

#[get("/game-models")]
async fn list_all_game_models(db: web::Data<Database>, query_params: Option<web::Query<GameModelRequest>>) -> Result<HttpResponse, CustomError> {
    let mut opt_active = None; // default

    if let Some(show_active) = query_params {
        opt_active = Some(show_active.active);
    }

    let found: Vec<GameModelDAO> = db.get_game_models(opt_active);

    Ok(HttpResponse::Ok().json(found))
}

#[get("/game-model/{id}")]
async fn get_game_model(db: web::Data<Database>, game_model_id: web::Path<i32>) -> HttpResponse {
    let extracted_id = game_model_id.into_inner();
    let found = db.get_game_model(extracted_id);

    match found {
        Some(found) => HttpResponse::Ok().json(found),
        None => HttpResponse::NotFound().body(format!("Game Model {:?} not found", extracted_id)),
    }
}

/// Returns a list of the Game Model Objects
///
/// # Arguments
/// * {db} the database object to run against
/// * {game_model_id} path of the game model
///
/// # Returns
#[get("/game-model/{game_id}/game-objects")]
async fn list_game_model_objects(db: web::Data<Database>, game_model_id: web::Path<i32>) -> Result<HttpResponse, CustomError> {
    let extracted_game_id = game_model_id.into_inner();
    let found = db.list_game_objects(extracted_game_id);

    Ok(HttpResponse::Ok().json(found))
}

/// Return a single GameModelObject if found
///
/// # Arguments
/// * {game_id} - ID of the target game
/// * {object_id} - ID of the GameModelObject for the game
///
/// # Returns
/// * {Result<HttpResponse, CustomError>} a packaged HTTP response with body of the object IF found. Error if not
#[get("/game-model/{game_id}/game-object/{object_id}")]
async fn get_game_model_object(db: web::Data<Database>, path_fields: web::Path<(i32, i32)>) -> Result<HttpResponse, CustomError> {
    let (extracted_game_id, extracted_model_object_id) = path_fields.into_inner();
    let found = db.get_game_model_object(extracted_game_id, extracted_model_object_id);
    match found {
        Some(found) => Ok(HttpResponse::Ok().json(found)),
        None => Ok(HttpResponse::NotFound().body(format!("Game Model Object {:?} not found", extracted_model_object_id))),
    }
}

/// Updates a single GameModelObject and associates with game
///
/// # Arguments
/// * {game_id} - ID of the target game
/// * {object_id} - ID of the GameModelObject for the game
///
/// # Returns
/// * {Result<HttpResponse, CustomError>} a packaged HTTP response with body of the object IF found. Error if not
#[post("/game-model/{game_id}/game-object/{object_id}")]
async fn update_game_model_object(db: web::Data<Database>, path_fields: web::Path<(i32, i32)>, target_payload: web::Json<GameModelObject>) -> Result<HttpResponse, CustomError> {
    let (extracted_game_id, extracted_model_object_id) = path_fields.into_inner();
    let found = db.get_game_model_object(extracted_game_id, extracted_model_object_id);
    let payload = target_payload.into_inner();

    // Match Target with two ids from request
    if payload.game_id != extracted_game_id {
        return Ok(HttpResponse::PreconditionFailed().body(format!("Game Model Object provided does not match the Game ID in the payload {:?}", extracted_game_id)));
    }
    if payload.id != extracted_model_object_id {
        return Ok(HttpResponse::PreconditionFailed().body(format!("Game Model Object IDs are immutable. The payload ID does not match the Web Path ID {:?}", extracted_model_object_id)));
    }

    if found.is_some() {
        debug!("existing has been found, this is the update flow");
    } else {
        debug!("item is not found, but create by default is enabled, so create a new one");
        return Ok(HttpResponse::NotFound().body(format!("Game Model Object {:?} not found", extracted_model_object_id)));
    }

    // for the most part, things are OK
    let updated = db.update_game_model_object(payload);

    match updated {
        Ok(updated_item) => {
            Ok(HttpResponse::Ok().json(updated_item))
        }
        Err(error) => Ok(HttpResponse::InternalServerError().body(format!("Game Model Object {:?} not updated", error))),
    }
}

/// Creates a single GameModelObject and associates with game
///
/// # Arguments
/// * {game_id} - ID of the target game
/// * {game-model-object} - JSON object of the new Game Model Object
///
/// # Returns
/// * {Result<HttpResponse, CustomError>} a packaged HTTP response with body of the object IF found. Error if not
#[put("/game-model/{game_id}/game-object")]
async fn create_game_model_object(db: web::Data<Database>, path_fields: web::Path<i32>, target_payload: web::Json<GameModelObject>) -> Result<HttpResponse, CustomError> {
    let extracted_game_id = path_fields.into_inner();
    let mut payload = target_payload.into_inner();
    payload.game_id = extracted_game_id;

    // for the most part, things are OK
    let created = db.create_game_model_object(payload);

    match created {
        Ok(updated_item) => Ok(HttpResponse::Created().json(updated_item)),
        Err(error) => Ok(HttpResponse::InternalServerError().body(format!("Game Model Object {:?} not updated", error))),
    }
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(list_all_game_models);
    config.service(get_game_model);
    config.service(list_game_model_objects);
    config.service(get_game_model_object);
    config.service(create_game_model_object);
    config.service(update_game_model_object);
}