use crate::models::MigrationItem;
use crate::repository::game_db::Database;
use actix_web::{get, web, HttpResponse, Responder};
use log::{debug, error};
use serde_derive::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
struct HealthCheck {
    health: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct Metadata {
    app_version: String,
    migrations: Option<Vec<MigrationItem>>,
}

#[get("/metadata")]
async fn metadata(db: web::Data<Database>) -> impl Responder {

    // get all migrations run
    let migrations = match db.get_migrations() {
        Ok(list) => {
            debug!("Migrations: {:?}", list);
            Some(list)
        }
        Err(err) => {
            error!("Failed to retrieve migrations: {:?}", err);
            None
        }
    };

    let health = Metadata {
        app_version: env!("CARGO_PKG_VERSION").to_string(),
        migrations,
    };
    HttpResponse::Ok().json(health)
}

#[get("/health")]
async fn health_check(db: web::Data<Database>) -> impl Responder {
    // check DB connection
    match db.pool.get() {
        Ok(_) => {
            let check = HealthCheck {
                health: true,
            };
            HttpResponse::Ok().json(check)
        }
        Err(_) => {
            let check = HealthCheck {
                health: false,
            };
            HttpResponse::InternalServerError().json(check)
        }
    }
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(health_check);
    config.service(metadata);
}