use actix_web::http::StatusCode;
use actix_web::web::Data;
#[allow(unused_imports)]
use actix_web::{delete, get, post, put, web, HttpResponse};
use chrono::DateTime;
use log::{debug, info, warn};
use rand::Rng;
use uuid::Uuid;

use crate::models::game::{GameDAO, GameScoreRecord};
use crate::models::game_model::NewGameRoundConfig;
use crate::repository::game_db::Database;

/// List all the games known in the system
///
/// # Arguments
/// * {db} - Database object to perform the action on
///
/// # Returns
/// * {HttpReponse} that can be StatusCode::OK or StatusCode::Error
#[get("/games")]
async fn list_all_games(db: Data<Database>) -> HttpResponse {
    let game_results = db.get_games(false);
    match game_results {
        Ok(game_results) => HttpResponse::Ok().json(game_results),
        Err(err) => {
            let status_code = StatusCode::from_u16(err.error_status_code).ok().unwrap();
            HttpResponse::build(status_code).body(format!("Error retrieving all games: {:?}", err))
        }
    }
}

#[get("/games/{id}")]
pub async fn get_game_by_id(db: Data<Database>, id: web::Path<String>) -> HttpResponse {
    let found_game = db.get_game(id.into_inner());

    match found_game {
        Ok(game) => HttpResponse::Ok().json(game.unwrap()),
        Err(err) => {
            let status_code = StatusCode::from_u16(err.error_status_code).ok().unwrap();
            HttpResponse::build(status_code).body(err.to_string())
        }
    }
}

#[get("/game/active")]
pub async fn get_first_active_game(db: Data<Database>) -> HttpResponse {
    let found_game = db.get_active_game();

    match found_game {
        Ok(active_game) => HttpResponse::Ok().json(active_game),
        Err(_) => HttpResponse::build(StatusCode::NOT_FOUND).body("No Active Games were found")
    }
}

/// Activate a game and deactivate all others
#[put("/game/activate/{game_id}")]
pub async fn activate_game(db: Data<Database>, id: web::Path<String>) -> HttpResponse {
    let found_game = db.set_active_game(id.into_inner());

    match found_game {
        Ok(active_game) => HttpResponse::Ok().json(active_game),
        Err(_) => HttpResponse::build(StatusCode::NOT_FOUND).body("No Active Games were found")
    }
}


#[post("/games")]
pub async fn create_game(db: Data<Database>, new_game: web::Json<GameDAO>) -> HttpResponse {
    let new_game = db.create_game(new_game.into_inner());

    match new_game {
        Ok(new_game) => HttpResponse::Ok().json(new_game),
        Err(err) => HttpResponse::InternalServerError().body(err.to_string()),
    }
}

#[delete("/games/{id}")]
pub async fn delete_game_by_id(db: Data<Database>, id: web::Path<String>) -> HttpResponse {
    let deleted_game = db.delete_game_by_id(id.into_inner());
    match deleted_game {
        Ok(count) => HttpResponse::Ok().body(format!("Removed {count}")),
        Err(_) => HttpResponse::NotFound().body("Game was not found and did not delete"),
    }
}

#[put("/games/{id}")]
pub async fn update_game_by_id(db: Data<Database>, id: web::Path<String>, target: web::Json<GameDAO>) -> HttpResponse {
    let game_dao = target.into_inner();
    let updated_game = db.set_game_status(id.into_inner(), game_dao.game_status);
    match updated_game {
        Ok(updated_game) => HttpResponse::Ok().json(updated_game),
        Err(e) => HttpResponse::InternalServerError().body(format!("{:?}", e)),
    }
}

// Get Game Rounds (by game) -- query parameters: by status w/ order (by insert date)
#[get("/games/{game_id}/rounds")]
pub async fn get_all_instances_by_game(db: Data<Database>, game_id: web::Path<String>) -> HttpResponse {
    let game_round_results = db.get_game_rounds_by_game(game_id.into_inner());
    match game_round_results {
        Ok(game_round) => {
            HttpResponse::Ok().json(game_round)
        }
        Err(err) => {
            HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
        }
    }
}

// Get Game Rounds (by game) -- query parameters: by status w/ order (by insert date)
#[delete("/games/{game_id}/rounds")]
pub async fn delete_all_rounds_by_game(db: Data<Database>, game_id: web::Path<String>) -> HttpResponse {
    let game_round_results = db.delete_all_rounds_by_game(game_id.into_inner());
    match game_round_results {
        Ok(_) => {
            HttpResponse::Created().finish()
        }
        Err(err) => {
            HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
        }
    }
}

#[get("/games/{game_id}/leaderboard")]
pub async fn get_leaderboard_by_game(db: Data<Database>, game_id: web::Path<String>) -> HttpResponse {
    let game_round_results = db.get_leaderboard_for_game(game_id.into_inner());
    match game_round_results {
        Ok(game_rounds) => {
            HttpResponse::Ok().json(game_rounds)
        }
        Err(err) => {
            HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
        }
    }
}

// Create Game Instance
#[post("/games/{game_id}/rounds")]
pub async fn create_game_instance(db: Data<Database>, game_id: web::Path<String>, target: web::Json<NewGameRoundConfig>) -> HttpResponse {
    let mut game_config = target.into_inner();

    let game_id_str = game_id.into_inner();
    let Ok(game_uuid) = Uuid::parse_str(game_id_str.as_str()) else {
        info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id_str);
        return HttpResponse::BadRequest().body("Invalid Game ID".to_string());
    };

    // Get the GameDAO for the provided game ID path
    match db.get_game(game_uuid.to_string()) {
        Ok(target_game) => {
            game_config.player_id = Uuid::new_v4();
            game_config.game_id = game_uuid;
            game_config.target = generate_round_target(target_game.unwrap());

            match db.create_game_round(game_config) {
                Ok(round) => HttpResponse::Ok().json(round),
                Err(err) => HttpResponse::InternalServerError().body(format!("Cannot update the game instance due to {:?}", err))
            }
        }
        Err(err) => {
            HttpResponse::NotFound().body(format!("Game ID not found while creating new game round instance. {:?}", err))
        }
    }
}

/// Need to take into account the max of each item in a game, the min of each item, then pick something
///     in between the max/min. For now, a range of 10 to 30 is sufficient
fn generate_round_target(dao: GameDAO) -> f64 {
    let mut rng = rand::thread_rng();

    let min = dao.min_target;
    let max = dao.max_target;
    debug!("Minimum target size {:.}", min);
    debug!("Max target size {:.}", max);

    let gen_value = rng.gen_range(min..max);
    debug!("Generated value of: {:.}", gen_value);
    gen_value
}

/// Record a new score for a game instance
#[post("/game-rounds/score")]
pub async fn record_new_score(db: Data<Database>, score_record: web::Json<GameScoreRecord>) -> HttpResponse {
    let mut record = score_record.into_inner();
    // validate Instance has been started
    let game_instance_id = record.game_round_id.to_string();
    if !is_game_instance_valid(&db, &game_instance_id) {
        return HttpResponse::NotFound().body("Game Instance was not found");
    }

    if record.created_at <= DateTime::UNIX_EPOCH.naive_utc() {
        record.created_at = chrono::Utc::now().naive_utc();
    }

    let score_result = db.record_score(record);

    match score_result {
        Ok(_) => HttpResponse::Ok().json("Recorded"),
        Err(err) => HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
    }
}

/// Record a new score for a game instance
#[put("/game-rounds/score")]
pub async fn update_round_score(db: Data<Database>, score_record: web::Json<GameScoreRecord>) -> HttpResponse {
    let record = score_record.into_inner();
    // validate Instance has been started
    let game_instance_id = record.game_round_id.to_string();
    if !is_game_instance_valid(&db, &game_instance_id) {
        return HttpResponse::NotFound().body("Game Instance was not found");
    }

    let score_result = db.update_score(record);

    match score_result {
        Ok(_) => HttpResponse::Ok().json("Recorded"),
        Err(err) => HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
    }
}

/// Returns the final score for the round
#[get("/game-rounds/{round_id}/final")]
pub async fn get_round_final_score(db: Data<Database>, path_vars: web::Path<String>) -> HttpResponse {
    let game_instance_id = path_vars.into_inner();

    debug!("Will look up final score for game-instance.player_id in Game {game_instance_id}");

    let result = db.get_final_score(game_instance_id);

    match result {
        Ok(score) => HttpResponse::Ok().json(score),
        Err(err) => HttpResponse::InternalServerError().body(format!("Error: {:?}", err))
    }
}

/// Get an Instance ID from a Game + Player
#[get("/game-rounds/game/{in_game_id}/player/{in_player_id}")]
pub async fn get_round_id_from_player_game(db: Data<Database>, path_params: web::Path<(String, String)>) -> HttpResponse {
    let (game_id_str, player_id_str) = path_params.into_inner();
    let result = db.get_instance_id_from_game_player(game_id_str, player_id_str);

    match result {
        Ok(was_success) => {
            match was_success {
                Some(item) => HttpResponse::Ok().json(item),
                None => HttpResponse::NotFound().json("Game Instance was not found"),
            }
        }
        Err(err) => HttpResponse::NotFound().json(format!("Game Instance ID was not found. {:?}", err))
    }
}

/// Shortcut to start a game instance (round) rather than using Game Instance Update
#[get("/game-scores-count/{game_id}")]
pub async fn get_score_count_by_game(db: Data<Database>, instance_id: web::Path<String>) -> HttpResponse {
    let game_id_str = instance_id.into_inner();

    let Ok(_) = Uuid::parse_str(game_id_str.as_str()) else {
        info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id_str);
        return HttpResponse::BadRequest().body("Invalid Game ID".to_string());
    };

    let result = db.get_leaderboard_for_game(game_id_str);

    match result {
        Ok(leaderboard_games) => {
            let count = leaderboard_games.len();
            HttpResponse::Ok().body(format!("{count}"))
        }
        Err(err) => HttpResponse::NotFound().json(format!("Game ID was not found. {:?}", err))
    }
}

/// Shortcut to start a game instance (round) rather than using Game Instance Update
#[post("/game-rounds/{round_id}/start")]
pub async fn start_game_round(db: Data<Database>, instance_id: web::Path<String>) -> HttpResponse {
    let game_instance_id = instance_id.into_inner();
    if !is_game_instance_valid(&db, &game_instance_id) {
        return HttpResponse::NotFound().body("Game Instance was not found");
    }
    // PLAYING
    let result = db.update_game_round_status(game_instance_id, "PLAYING".to_string());
    match result {
        Ok(was_success) => HttpResponse::Ok().json(format!("Started = {was_success}")),
        Err(err) => HttpResponse::NotFound().json(format!("Game Instance ID was not found. {:?}", err))
    }
}

/// Shortcut to stop a game instance (round)
#[post("/game-rounds/{round_id}/stop")]
pub async fn stop_game_round(db: Data<Database>, instance_id: web::Path<String>) -> HttpResponse {
    let game_instance_id = instance_id.into_inner();
    if !is_game_instance_valid(&db, &game_instance_id) {
        return HttpResponse::NotFound().body("Game Instance was not found");
    }
    // FINISHED
    let result = db.update_game_round_status(game_instance_id, "FINISHED".to_string());
    match result {
        Ok(was_success) => HttpResponse::Ok().json(format!("Stopped = {was_success}")),
        Err(err) => HttpResponse::NotFound().json(format!("Game Instance ID was not found. {:?}", err))
    }
}

pub fn init_routes(config: &mut web::ServiceConfig) {
    config.service(list_all_games);
    config.service(get_game_by_id);
    config.service(create_game);
    config.service(update_game_by_id);
    config.service(delete_game_by_id);
    // Game Instances
    config.service(get_all_instances_by_game);
    config.service(create_game_instance);
    config.service(get_first_active_game);
    config.service(activate_game);
    config.service(get_leaderboard_by_game);
    config.service(get_score_count_by_game);
    // Game Instance Operations
    config.service(record_new_score);
    config.service(update_round_score);
    config.service(get_round_final_score);
    config.service(delete_all_rounds_by_game);
    // Instance control
    config.service(get_round_id_from_player_game);
    config.service(stop_game_round);
    config.service(start_game_round);
}

fn is_game_instance_valid(db: &Data<Database>, instance_id: &String) -> bool {
    let game_instance = db.get_game_round(instance_id.clone());

    let Ok(game_instance_result) = game_instance else {
        warn!("Game Instance Result {} was not found", instance_id);
        return false;
    };

    let Some(_) = game_instance_result else {
        warn!("Game Instance {} was empty", instance_id);
        return false;
    };

    return true;
}
