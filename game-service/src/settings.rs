use std::env;

use config::{Config, ConfigError, File};
use log::info;
use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct GameServiceConfig {
    pub server: GameServerConfig,
    pub database: DatabaseConfig,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct DatabaseConfig {
    pub dbtype: String,
    pub host: String,
    pub port: String,
    pub user: String,
    pub password: String,
    pub database: String,
    pub log_queries: bool,
}

impl DatabaseConfig {
    pub fn connect_string(&self) -> String {
        let pass = env::var("DB_PASS").unwrap_or(self.password.clone());
        let user = env::var("DB_USER").unwrap_or(self.user.clone());
        format!("{}://{}:{}@{}:{}/{}", self.dbtype, user, pass, self.host, self.port, self.database)
    }
}

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct GameServerConfig {
    pub port: String,
}

#[derive(Debug, Deserialize)]
#[allow(unused)]
pub struct Settings {
    pub debug: bool,
    #[serde(alias = "service")]
    pub game_service_config: GameServiceConfig,
}

impl Settings {
    pub fn new() -> Result<Self, ConfigError> {
        let run_mode = env::var("RUN_MODE").unwrap_or_else(|_| "production".into());
        // ENV variable has the base folder for the config --  APP_CONFIG_FOLDER
        let folder_base = env::var("APP_CONFIG_FOLDER").unwrap_or("config/".to_string());
        let s = Config::builder()
            .add_source(File::with_name(&format!("{folder_base}/default")))
            .add_source(File::with_name(&format!("{folder_base}/{run_mode}")).required(false))
            .build()?;

        info!("Settings configuration file: {}", run_mode);

        s.try_deserialize()
    }
}
