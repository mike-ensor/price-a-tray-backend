use actix_web::http::StatusCode;
use diesel::prelude::*;
use log::{error, info};

use web_utils::error_handler::CustomError;

use crate::models::game_model::{GameModelDAO, GameModelObject};
use crate::repository::game_db::Database;

/// Game Model Objects are the items used in a game
impl Database {
    /// List all GameModels
    pub fn get_game_models(&self, active_param: Option<bool>) -> Vec<GameModelDAO> {
        use crate::repository::schema::game_model::active;
        use crate::repository::schema::game_model::dsl::game_model;

        let mut active_default = true;
        if let Some(active_param) = active_param {
            active_default = active_param;
        }

        game_model
            .filter(active.eq(active_default))
            .load::<GameModelDAO>(&mut self.pool.get().unwrap())
            .expect("Error loading active game models")
    }

    /// Get a single GameModel object
    ///
    /// # Arguments
    /// * {model_id} the specific ID of the GameModel
    ///
    /// # Returns
    /// * Option<GameModelDAO>
    pub fn get_game_model(&self, model_id: i32) -> Option<GameModelDAO> {
        use crate::repository::schema::game_model::id;
        use crate::repository::schema::game_model::dsl::game_model;

        game_model
            .filter(id.eq(model_id))
            .first::<GameModelDAO>(&mut self.pool.get().unwrap())
            .ok()
    }

    /// Lists all the playable objects in the game
    ///
    /// # Arguments
    /// * {target_game_id} the target Game ID
    ///
    /// # Returns
    /// * {Vec<GameModelObject>} a vector of all the GameItemsObjects associated with the game
    pub fn list_game_objects(&self, target_game_id: i32) -> Vec<GameModelObject> {
        use crate::repository::schema::game_model_object::game_id;
        use crate::repository::schema::game_model_object::dsl::game_model_object;

        game_model_object
            .filter(game_id.eq(target_game_id))
            .load::<GameModelObject>(&mut self.pool.get().unwrap())
            .expect("Error loading Objects for game")
    }

    /// Get a single GameModelObject (ie: the physical item)
    ///
    /// # Arguments
    /// * {target_game_id) the GameID that the GameModelObject is associated with
    /// * {game_model_object_id} the ID of the specific GameModelObject
    pub fn get_game_model_object(&self, target_game_id: i32, game_model_object_id: i32) -> Option<GameModelObject> {
        use crate::repository::schema::game_model_object::game_id;
        use crate::repository::schema::game_model_object::id;
        use crate::repository::schema::game_model_object::dsl::game_model_object;

        let result = game_model_object
            .filter(game_id.eq(target_game_id))
            .filter(id.eq(game_model_object_id))
            .get_result::<GameModelObject>(&mut self.pool.get().unwrap());

        match result {
            Ok(result) => Some(result),
            Err(err) => {
                error!("Error getting a model object by ID: {err}");
                None
            }
        }
    }

    /// Update a single GameModelObject (ie: the physical item)
    ///
    /// # Arguments
    /// * {target_game_id) the GameID that the GameModelObject is associated with
    /// * {game_model_object_id} the ID of the specific GameModelObject
    pub fn update_game_model_object(&self, game_object: GameModelObject) -> Result<GameModelObject, CustomError> {
        // Fields to update & use in query
        use crate::repository::schema::game_model_object::game_id;
        use crate::repository::schema::game_model_object::id;
        use crate::repository::schema::game_model_object::lower;
        use crate::repository::schema::game_model_object::upper;
        use crate::repository::schema::game_model_object::model_label;
        use crate::repository::schema::game_model_object::name;
        use crate::repository::schema::game_model_object::image_link;

        // table object
        use crate::repository::schema::game_model_object::dsl::game_model_object;

        info!("Target Game ID to update {:?}", game_object.game_id);
        info!("Game Object new Lower {:?}", game_object.lower);
        info!("Game Object new Upper {:?}", game_object.upper);

        let values = (
            id.eq(game_object.id),
            name.eq(game_object.name),
            game_id.eq(game_object.game_id),
            lower.eq(game_object.lower),
            model_label.eq(game_object.model_label),
            upper.eq(game_object.upper),
            image_link.eq(game_object.image_link),
        );

        let db_op_result = diesel::update(game_model_object.find(game_object.id))
            .set(values)
            .get_result::<GameModelObject>(&mut self.get_connection());

        match db_op_result {
            Ok(result) => Ok(result),
            Err(err) => {
                error!("Error getting a model object by ID: {err}");
                Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), format!("Cannot update the GameModelObject due to: {:?}", err)))
            }
        }
    }

    /// Create a single GameModelObject (ie: the physical item)
    ///
    /// # Arguments
    /// * {target_game_id) the GameID that the GameModelObject is associated with
    /// * {game_model_object_id} the ID of the specific GameModelObject
    pub fn create_game_model_object(&self, game_object: GameModelObject) -> Result<GameModelObject, CustomError> {
        use crate::repository::schema::game_model_object::game_id;
        use crate::repository::schema::game_model_object::lower;
        use crate::repository::schema::game_model_object::upper;
        use crate::repository::schema::game_model_object::model_label;
        use crate::repository::schema::game_model_object::image_link;
        use crate::repository::schema::game_model_object::name;

        // table object
        use crate::repository::schema::game_model_object::dsl::game_model_object;

        info!("Target Game ID to update {:?}", game_object.game_id);
        info!("Game Object new Lower {:?}", game_object.lower);
        info!("Game Object new Upper {:?}", game_object.upper);

        // Recreate the Object
        let values = (
            name.eq(game_object.name),
            game_id.eq(game_object.game_id),
            lower.eq(game_object.lower),
            model_label.eq(game_object.model_label),
            upper.eq(game_object.upper),
            image_link.eq(game_object.image_link),
        );

        // NOTE: The default insert is not working due to the ID not being violation of PK. Not sure why SERIAL isn't working
        let db_op_result = diesel::insert_into(game_model_object)
            .values(&values)
            .get_result::<GameModelObject>(&mut self.get_connection());

        match db_op_result {
            Ok(result) => {
                info!("Game Object ID (newly created) {:?}", result.id);
                Ok(result)
            },
            Err(err) => {
                error!("Error saving a new Game Model Object: {err}");
                Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), format!("Cannot update the GameModelObject due to: {:?}", err)))
            }
        }
    }
}