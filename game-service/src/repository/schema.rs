// @generated automatically by Diesel CLI.

diesel::table! {
    game_accelerator (id) {
        id -> Int4,
        #[max_length = 128]
        name -> Varchar,
        description -> Nullable<Text>,
    }
}

diesel::table! {
    game_model (id) {
        id -> Int4,
        #[max_length = 128]
        short_name -> Varchar,
        description -> Nullable<Text>,
        last_trained -> Timestamp,
        #[max_length = 255]
        source -> Varchar,
        active -> Bool,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
    }
}

diesel::table! {
    game_model_object (id) {
        id -> Int4,
        #[max_length = 128]
        name -> Varchar,
        lower -> Float8,
        upper -> Float8,
        #[max_length = 255]
        image_link -> Nullable<Varchar>,
        created_at -> Nullable<Timestamp>,
        updated_at -> Nullable<Timestamp>,
        game_id -> Int4,
        #[max_length = 64]
        model_label -> Varchar,
    }
}

diesel::table! {
    game_round_status (id) {
        #[max_length = 64]
        id -> Varchar,
    }
}

diesel::table! {
    game_rounds (id) {
        id -> Uuid,
        player_id -> Uuid,
        game_id -> Uuid,
        #[max_length = 64]
        status -> Varchar,
        target -> Float8,
        game_runtime -> Int4,
        game_accelerator -> Int4,
        start_time -> Nullable<Timestamp>,
        end_time -> Nullable<Timestamp>,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

diesel::table! {
    game_runtime (id) {
        id -> Int4,
        #[max_length = 128]
        name -> Varchar,
        description -> Nullable<Text>,
    }
}

diesel::table! {
    game_scores (id) {
        id -> Int4,
        game_round_id -> Uuid,
        score -> Float8,
        target -> Float8,
        #[max_length = 50]
        initials -> Nullable<Varchar>,
        created_at -> Timestamp,
    }
}

diesel::table! {
    game_status (status) {
        #[max_length = 64]
        status -> Varchar,
        description -> Nullable<Text>,
    }
}

diesel::table! {
    games (id) {
        id -> Uuid,
        #[max_length = 64]
        name -> Varchar,
        description -> Nullable<Text>,
        #[max_length = 128]
        event_name -> Nullable<Varchar>,
        active -> Bool,
        model_id -> Nullable<Int4>,
        #[max_length = 64]
        game_status -> Varchar,
        created_at -> Timestamp,
        updated_at -> Timestamp,
        #[max_length = 10]
        locale -> Varchar,
        round_length -> Int4,
        min_target -> Float8,
        max_target -> Float8,
    }
}

diesel::joinable!(game_model_object -> game_model (game_id));
diesel::joinable!(game_rounds -> game_accelerator (game_accelerator));
diesel::joinable!(game_rounds -> game_round_status (status));
diesel::joinable!(game_rounds -> game_runtime (game_runtime));
diesel::joinable!(game_rounds -> games (game_id));
diesel::joinable!(game_scores -> game_rounds (game_round_id));
diesel::joinable!(games -> game_model (model_id));
diesel::joinable!(games -> game_status (game_status));

diesel::allow_tables_to_appear_in_same_query!(
    game_accelerator,
    game_model,
    game_model_object,
    game_round_status,
    game_rounds,
    game_runtime,
    game_scores,
    game_status,
    games,
);
