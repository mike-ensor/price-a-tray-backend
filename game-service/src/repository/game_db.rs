use actix_web::http::StatusCode;
use chrono::{Duration, Utc};
use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use diesel::sql_query;
use log::{debug, error, info};
use r2d2::PooledConnection;
use uuid::Uuid;

use web_utils::error_handler::CustomError;

use crate::models::game::{GameDAO, GameScoreRecord};
use crate::models::game_model::{GameRoundDAO, NewGameRoundConfig};
use crate::models::MigrationItem;
use crate::repository::schema::game_rounds::dsl::game_rounds;
use crate::repository::schema::game_scores::dsl::game_scores;
use crate::repository::schema::games::dsl::*;
use crate::repository::schema::games::dsl::games;
use crate::settings::DatabaseConfig;

type DBPool = r2d2::Pool<ConnectionManager<PgConnection>>;

pub struct Database {
    pub(crate) pool: DBPool,
}

impl Database {
    pub fn new(config: DatabaseConfig) -> Self {
        let database_url = config.connect_string();
        let manager = ConnectionManager::<PgConnection>::new(database_url);
        let pool: DBPool = r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");
        Database { pool }
    }

    /// Get all active games returned unless there is a DB error
    /// TODO: Update this to Vec<Game> (need to inner_join the game.model_id to game_models.id
    pub fn get_games(&self, only_active: bool) -> Result<Vec<GameDAO>, CustomError> {
        let result;
        if only_active {
            result = games
                .filter(active.eq(true))
                .order(event_name.asc())
                .load::<GameDAO>(&mut self.get_connection());
        } else {
            result = games
                .order(event_name.asc())
                .load::<GameDAO>(&mut self.get_connection());
        }

        match result {
            Ok(returned) => Ok(returned),
            Err(err) => Err(CustomError::new(500, format!("Error retreving games. {:?}", err))),
        }
    }

    /// Get the current active game
    /// TODO: Future, maybe return "true" so more than 1 game can be "active", then control which game is in-scope
    ///       with the UI
    pub fn get_active_game(&self) -> Result<GameDAO, CustomError> {
        let result = games
            .filter(active.eq(true))
            .first::<GameDAO>(&mut self.get_connection());
        match result {
            Ok(returned) => Ok(returned),
            Err(err) => Err(CustomError::new(500, format!("Error getting the active game. {:?}", err))),
        }
    }

    /// Deactivate games and activate the incoming one
    /// TODO: See get_active_game
    pub fn set_active_game(&self, game_id: String) -> Result<GameDAO, CustomError> {
        // Quick approach, update all Games setting "active = false"
        //    then update specific game with active=true
        let Ok(uuid) = Uuid::parse_str(game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id);
            return Err(CustomError::new(StatusCode::BAD_REQUEST.as_u16(), "UUID is incorrect".to_string()));
        };

        let set_all_inactive = diesel::update(games)
            .set(active.eq(false))
            .get_results::<GameDAO>(&mut self.get_connection());
        match set_all_inactive {
            Ok(_) => {
                let values = (active.eq(true), updated_at.eq(Utc::now().naive_utc()));

                let result = diesel::update(games.find(uuid))
                    .set(values)
                    .get_result::<GameDAO>(&mut self.get_connection());

                match result {
                    Ok(returned) => Ok(returned),
                    Err(err) => Err(CustomError::new(500, format!("Error setting the active game. {:?}", err))),
                }
            }
            Err(err) => Err(CustomError::new(500, format!("Error setting the all games to inactive. {:?}", err))),
        }
    }

    /// Crate a single game and return the newly created object or an error
    pub fn create_game(&self, target_game: GameDAO) -> Result<GameDAO, CustomError> {
        let mut new_uuid = Uuid::new_v4();
        if !target_game.id.is_nil() {
            new_uuid = target_game.id;
        }

        debug!("UUID on POST: {:?}", new_uuid);
        let target_game = GameDAO {
            id: new_uuid,
            created_at: Utc::now().naive_utc(),
            updated_at: Utc::now().naive_utc(),
            ..target_game
        };
        let result = diesel::insert_into(games)
            .values(&target_game)
            .execute(&mut self.get_connection());
        match result {
            Ok(_) => Ok(target_game),
            Err(err) => Err(CustomError::new(500, format!("Error creating game. {:?}", err)))
        }
    }

    /// Get a single game and return Result + Option<GameDAO> if error and/or result
    pub fn get_game(&self, game_id: String) -> Result<Option<GameDAO>, CustomError> {
        // if the UUID is valid, then search
        let Ok(uuid) = Uuid::parse_str(game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id);
            return Err(CustomError::new(StatusCode::BAD_REQUEST.as_u16(), "UUID is incorrect".to_string()));
        };

        let found_game = games
            .find(uuid)
            .get_result::<GameDAO>(&mut self.get_connection());

        match found_game {
            Ok(found_game) => Ok(Some(found_game)),
            Err(err) => Err(CustomError::new(404, format!("Game not found. {:?}", err))),
        }
    }

    /// Delete a game, return successful completion option
    pub fn delete_game_by_id(&self, game_id: String) -> Result<usize, CustomError> {
        let Ok(uuid) = Uuid::parse_str(game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id);
            return Err(CustomError::new(404, "Game not found".to_string()));
        };

        let result = diesel::delete(games.find(uuid))
            .execute(&mut self.get_connection());

        match result {
            Ok(count) => Ok(count),
            Err(err) => {
                Err(CustomError::new(404, format!("Game not found. {:?}", err)))
            }
        }
    }

    /// Only sets the Game Status field on the Game
    pub fn set_game_status(&self, game_id: String, game_status_state: String) -> Result<Option<GameDAO>, CustomError> {
        let Ok(uuid) = Uuid::parse_str(game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID", game_id);
            return Err(CustomError::new(415, "UUID is not a proper format. Game was not updated".to_string()));
        };

        let result_updated_game = diesel::update(games.find(uuid))
            .set(game_status.eq(game_status_state))
            // .set(updated_at.eq(Utc::now().naive_utc()))
            .get_result::<GameDAO>(&mut self.get_connection());

        match result_updated_game {
            Ok(game) => Ok(Some(game)),
            Err(err) => Err(CustomError::new(500, format!("Cannot update game status. {:?}", err))),
        }
    }

    /// This method will update a Game object to what is sent in (CAUTION, ALL fields will try to be matched and set)
    pub fn _update_game_by_id(&self, game_id: String, target: GameDAO) -> Result<Option<GameDAO>, CustomError> {
        let Ok(uuid) = Uuid::parse_str(game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", game_id);
            return Err(CustomError::new(415, "UUID is not a proper format. Game was not updated".to_string()));
        };

        let updated_game = diesel::update(games.find(uuid))
            .set(&target)
            .get_result::<GameDAO>(&mut self.get_connection());

        match updated_game {
            Ok(game) => Ok(Some(game)),
            Err(err) => Err(CustomError::new(500, format!("Cannot create game. {:?}", err))),
        }
    }

    /// This method will update a Game object to what is sent in (CAUTION, ALL fields will try to be matched and set)
    pub fn delete_all_rounds_by_game(&self, in_game_id: String) -> Result<(), CustomError> {
        use crate::repository::schema::game_rounds::game_id;

        let Ok(uuid) = Uuid::parse_str(in_game_id.as_str()) else {
            info!("Game Instance Game ID: {:?} was not a proper UUID", in_game_id);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let updated_game = diesel::delete(game_rounds)
            .filter(game_id.eq(uuid))
            .execute(&mut self.get_connection());

        match updated_game {
            Ok(_) => Ok(()),
            Err(err) => Err(CustomError::new(500, format!("Cannot create game. {:?}", err))),
        }
    }

    pub fn get_game_rounds_by_game(&self, contain_game_id: String) -> Result<Vec<GameRoundDAO>, CustomError> {
        use crate::repository::schema::game_rounds::game_id;
        use crate::repository::schema::game_rounds::start_time;

        let Ok(uuid) = Uuid::parse_str(contain_game_id.as_str()) else {
            info!("Path Game ID: {:?} was not a proper UUID and therefore can also not be a valid game ID", contain_game_id);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let found_games = game_rounds
            .filter(game_id.eq(uuid))
            .order_by(start_time.asc())
            .get_results(&mut self.get_connection());

        match found_games {
            Ok(items) => Ok(items),
            Err(err) => Err(CustomError::new(500, format!("Error: {:?}", err))),
        }
    }

    pub fn create_game_round(&self, mut config: NewGameRoundConfig) -> Result<GameRoundDAO, CustomError> {
        if config.status.is_empty() {
            config.status = "REGISTERED".to_string();
        }

        let returned: QueryResult<GameRoundDAO> = diesel::insert_into(game_rounds)
            .values(&config)
            .get_result(&mut self.get_connection());

        match returned {
            Ok(item) => {
                Ok(item)
            }
            Err(err) => {
                let msg = format!("Error creating game instance: {:?}", err);
                error!("{msg}");
                Err(CustomError::new(StatusCode::BAD_REQUEST.as_u16(), msg))
            }
        }
    }

    pub fn get_game_round(&self, game_round_id: String) -> Result<Option<GameRoundDAO>, CustomError> {
        use crate::repository::schema::game_rounds::id;

        let Ok(uuid) = Uuid::parse_str(game_round_id.as_str()) else {
            info!("Game Instance Game ID: {:?} was not a proper UUID", game_round_id);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let found_game = game_rounds
            .filter(id.eq(uuid))
            .get_result::<GameRoundDAO>(&mut self.get_connection());

        match found_game {
            Ok(found_game_round) => Ok(Some(found_game_round)),
            Err(err) => Err(CustomError::new(404, format!("Game Instance not found. {:?}", err))),
        }
    }

    /// Returns a Game Instance ID based on the Game and Player ID (return just the UUID or a GameInstance?)
    pub fn get_instance_id_from_game_player(&self, game_ident: String, player_ident: String) -> Result<Option<GameRoundDAO>, CustomError> {
        use crate::repository::schema::game_rounds::game_id;
        use crate::repository::schema::game_rounds::player_id;
        use crate::repository::schema::game_rounds::updated_at;

        let Ok(game_uuid) = Uuid::parse_str(game_ident.as_str()) else {
            info!("Game ID: {:?} was not a proper UUID", game_ident);
            return Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), "Error: Game UUID was not properly formatted".to_string()));
        };

        let Ok(player_uuid) = Uuid::parse_str(player_ident.as_str()) else {
            info!("Player ID: {:?} was not a proper UUID", player_ident);
            return Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), "Error: Player UUID was not properly formatted".to_string()));
        };

        let found_game = game_rounds
            .filter(game_id.eq(game_uuid))
            .filter(player_id.eq(player_uuid))
            .order_by(updated_at.desc())
            .limit(1)
            .get_result::<GameRoundDAO>(&mut self.get_connection());

        match found_game {
            Ok(found_game_round) => {
                Ok(Some(found_game_round))
            }
            Err(err) => Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), format!("Game Instance not found. {:?}", err)))
        }
    }

    pub fn update_game_round_status(&self, game_round_id: String, game_round_status: String) -> Result<bool, CustomError> {
        use crate::repository::schema::game_rounds::status;
        use crate::repository::schema::game_rounds::end_time;
        use crate::repository::schema::game_rounds::start_time;

        let Ok(uuid) = Uuid::parse_str(game_round_id.as_str()) else {
            info!("Game Instance Game ID: {:?} was not a proper UUID", game_round_id);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let current_game_round_request = self.get_game_round(game_round_id);

        let Ok(current_game_round_option) = current_game_round_request else {
            return Err(CustomError::new(404, "Game Instance not found by ID".to_string()));
        };

        let Some(current_game_round) = current_game_round_option else {
            return Err(CustomError::new(404, "Game Instance not found with ID".to_string()));
        };

        let mut set_start_time = current_game_round.start_time;
        let mut set_end_time = current_game_round.end_time;

        // this runs on the "finished" as well, so need to query the existing state
        if game_round_status.eq("PLAYING") {
            let calc_start = Utc::now().naive_utc();
            set_start_time = Some(calc_start.clone());
            let calc_end = calc_start + Duration::seconds(30);
            set_end_time = Some(calc_end);
        }

        let values = (
            status.eq(game_round_status),
            start_time.eq(set_start_time),
            end_time.eq(set_end_time)
        );

        let updated_game_round = diesel::update(game_rounds.find(uuid))
            .set(values)
            .get_result::<GameRoundDAO>(&mut self.get_connection());

        match updated_game_round {
            Ok(_) => Ok(true),
            Err(err) => Err(CustomError::new(404, format!("Game Instance not found. {:?}", err))),
        }
    }

    /// Calculate and return a final score for a Game-Instance
    ///  1. Get the game rules (what the round/instance time length is)
    //   2. Get the Target Score - Score just before or just at the end time ( OrderBy created_date DESC, WHERE created_date <= End Time
    //   3. Do something with this value
    pub fn get_final_score(&self, in_game_round_id: String) -> Result<GameScoreRecord, CustomError> {
        use crate::repository::schema::game_scores::game_round_id;
        use crate::repository::schema::game_scores::created_at;
        use crate::repository::schema::game_rounds::id;

        let Ok(uuid) = Uuid::parse_str(in_game_round_id.as_str()) else {
            info!("Game Instance Game ID: {:?} was not a proper UUID", game_round_id);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let connection = &mut self.get_connection();

        let game_round = game_rounds
            .filter(id.eq(uuid))
            .get_result::<GameRoundDAO>(connection);

        let Ok(_game_round_obj) = game_round else {
            return Err(CustomError::new(404, "Game Instance not found".to_string()));
        };

        let result = game_scores
            .filter(game_round_id.eq(uuid))
            .order_by(created_at.desc())
            .first::<GameScoreRecord>(connection);

        match result {
            Ok(score) => Ok(score),
            Err(err) => Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), format!("Error finding scores. {:?}", err)))
        }
    }

    /// Records a score for a game-instance
    pub fn record_score(&self, score_record: GameScoreRecord) -> Result<GameScoreRecord, CustomError> {
        use crate::repository::schema::game_scores::game_round_id;
        use crate::repository::schema::game_scores::score;
        use crate::repository::schema::game_scores::target;
        use crate::repository::schema::game_scores::initials;
        use crate::repository::schema::game_scores::created_at;

        let values = (
            game_round_id.eq(score_record.game_round_id),
            score.eq(score_record.score),
            target.eq(score_record.target),
            initials.eq(score_record.initials),
            created_at.eq(score_record.created_at)
        );

        // TODO: Add a check for 'is game_round_status == "PLAYING"'  and fail if not

        let returned = diesel::insert_into(game_scores)
            .values(values)
            .get_result(&mut self.get_connection());

        match returned {
            Ok(item) => Ok(item),
            Err(err) => {
                let msg = format!("Error Adding Score for Instance: {:?}", err);
                Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), msg))
            }
        }
    }

    pub fn update_score(&self, score_record: GameScoreRecord) -> Result<GameScoreRecord, CustomError> {
        use crate::repository::schema::game_scores::game_round_id;
        use crate::repository::schema::game_scores::initials;

        let values = (
            game_round_id.eq(score_record.game_round_id),
            initials.eq(score_record.initials),
        );

        let returned = diesel::update(game_scores)
            .filter(game_round_id.eq(score_record.game_round_id))
            .set(values)
            .get_result(&mut self.get_connection());

        match returned {
            Ok(item) => Ok(item),
            Err(err) => {
                let msg = format!("Error Updating Score for Instance: {:?}", err);
                Err(CustomError::new(StatusCode::INTERNAL_SERVER_ERROR.as_u16(), msg))
            }
        }
    }

    /// Returns the leaderboard, all GameScores for a Game in score ascending order
    pub fn get_leaderboard_for_game(&self, game_identifier: String) -> Result<Vec<GameScoreRecord>, CustomError> {
        let Ok(uuid) = Uuid::parse_str(game_identifier.as_str()) else {
            info!("Game Instance Game ID: {:?} was not a proper UUID", game_identifier);
            return Err(CustomError::new(500, "Error: UUID was not properly formatted".to_string()));
        };

        let game_scores_by_game = sql_query("SELECT t.id, t.game_round_id, t.score, t.target, t.initials, t.created_at
            FROM game_scores t
            INNER JOIN game_rounds as gs on t.game_round_id = gs.id
            WHERE gs.game_id=$1
            ORDER BY t.score ASC
            ");

        let results = game_scores_by_game
            .bind::<diesel::sql_types::Uuid, _>(uuid)
            .get_results::<GameScoreRecord>(&mut self.get_connection());

        match results {
            Ok(leaderboard) => Ok(leaderboard),
            Err(err) => Err(CustomError::new(500, format!("Error: returning leaderboard: {err}")))
        }
    }

    /// Get migrations
    pub fn get_migrations(&self) -> Result<Vec<MigrationItem>, CustomError> {

        let migrations_run_list = sql_query(
            "SELECT version, run_on
                    FROM __diesel_schema_migrations
                    ORDER BY run_on ASC
            ");

        // : QueryResult<Vec<MigrationItem>>
        let results = migrations_run_list
            .get_results::<MigrationItem>(&mut self.get_connection());

        match results {
            Ok(migrations) => Ok(migrations),
            Err(err) => Err(CustomError::new(500, format!("Error returning migrations: {err}")))
        }
    }
}

impl Database {
    pub fn get_connection(&self) -> PooledConnection<ConnectionManager<PgConnection>> {
        self.pool.get().unwrap()
    }
}

// pub trait LoggingQueries {
//     /// Returns a DB connection from the pool wrapped with a LoggingConnection object
//     fn get_connection(&self) -> LoggingConnection<PooledConnection<ConnectionManager<PgConnection>>>;
//
//     fn instrumentation(&mut self) -> &mut (dyn Instrumentation + 'static) { todo!() }
//
//     // fn set_instrumentation<impl Instrumentation>(&mut self, _: impl Instrumentation) where impl dyn Instrumentation: Instrumentation { todo!() }
// }
