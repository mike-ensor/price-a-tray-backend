use std::io;
use actix_cors::Cors;

use actix_files::NamedFile;
use actix_web::{App, get, HttpResponse, HttpServer, middleware, Responder, web};
use log::{info};
use serde::Serialize;

use web_utils::error_handler::CustomError;

use crate::settings::Settings;

mod api;
mod models;
mod repository;

mod settings;

#[derive(Serialize)]
pub struct Response {
    pub message: String,
}

#[get("/favicon.ico")]
async fn favicon_ico() -> Result<impl Responder, CustomError> {
    // YES, this is a 100% dupe. Cannot figure out how to add two routes, or wildcard off the suffix, or call the other method
    let favicon_file = NamedFile::open("static/game-service-logo.ico");
    if favicon_file.is_err() {
        Err(CustomError::new(
            404,
            "Favicon file not found".to_string(),
        ))
    } else {
        Ok(favicon_file)
    }
}

#[get("/favicon")]
async fn favicon() -> Result<impl Responder, CustomError> {
    let favicon_file = NamedFile::open("static/game-service-logo.ico");
    if favicon_file.is_err() {
        Err(CustomError::new(
            404,
            "Favicon file not found".to_string(),
        ))
    } else {
        Ok(favicon_file)
    }
}

async fn not_found() -> Result<HttpResponse, CustomError> {
    let response = Response {
        message: "Resource not found".to_string(),
    };
    Ok(HttpResponse::NotFound().json(response))
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
    let settings = Settings::new().unwrap();

    let mut log_queries_level = log::LevelFilter::Info;
    if settings.game_service_config.database.log_queries {
        log_queries_level = log::LevelFilter::Debug;
    }

    pretty_env_logger::formatted_timed_builder()
        .parse_env("RUST_LOG")
        .filter(Some("diesel_logger"), log_queries_level)
        .init();


    let port = settings.game_service_config.server.port.clone();

    let database_config = settings.game_service_config.database.clone();

    let database = repository::game_db::Database::new(database_config);
    let app_data = web::Data::new(database);

    info!("Starting Price a Tray");
    info!("===============================");
    // TODO: Add more start status info here
    info!("===============================");

    let server = HttpServer::new(move || {
        let cors = Cors::permissive();

        App::new()
            .configure(api::game_routes::init_routes)
            .configure(api::common_routes::init_routes)
            .configure(api::game_model_routes::init_routes)
            .service(favicon)
            .service(favicon_ico)
            .default_service(web::route().to(not_found))
            .app_data(app_data.clone())
            .wrap(middleware::Logger::default())
            .wrap(cors)
    });

    info!("Server started on port {port}");

    server.bind(format!("0.0.0.0:{port}"))?
        .run()
        .await
}
