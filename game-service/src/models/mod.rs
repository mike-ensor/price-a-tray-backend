use chrono::NaiveDateTime;
use diesel::sql_types::*;
use diesel::{QueryableByName};
use serde_derive::{Deserialize, Serialize};

pub(crate) mod game;
pub(crate) mod game_model;

#[derive(Serialize, Deserialize, Debug, Clone, QueryableByName)]
#[diesel(table_name = __diesel_schema_migrations)]
pub(crate) struct MigrationItem {
    #[diesel(sql_type = Text)]
    version: String,
    #[diesel(sql_type = Timestamp)]
    run_on: NaiveDateTime
}
