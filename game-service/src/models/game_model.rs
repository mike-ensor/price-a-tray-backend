use chrono::{NaiveDateTime, Utc};

use diesel::{AsChangeset, Insertable, Queryable};
use serde_derive::{Deserialize, Serialize};

// Primarily for Insertable
use diesel::prelude::*;
use uuid::Uuid;

/// GameModelDAO is a data model represents the ML Model used by a game.
#[derive(Serialize, Deserialize, Debug, Selectable, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::game_model)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameModelDAO {
    #[serde(default)]
    pub id: i32,
    pub short_name: String,
    pub description: Option<String>,
    pub last_trained: NaiveDateTime,
    pub source: String,
    pub active: bool,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ModelAccelerator {
    pub id: i32,
    pub name: String,
}
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct GameModel {
    pub id: i32,
    pub short_name: String,
    pub description: Option<String>,
    pub last_trained: NaiveDateTime,
    pub source: String,
    pub active: bool,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
}

impl GameModelDAO {
    pub fn new(short_name: String, source: String, active: bool, description: Option<String>) -> Self {
        Self {
            id: 0,
            short_name,
            description,
            source,
            last_trained: Utc::now().naive_utc(),
            created_at: Some(Utc::now().naive_utc()),
            updated_at: Some(Utc::now().naive_utc()),
            active,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Selectable, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::game_model_object)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameModelObject {
    #[serde(default)]
    pub id: i32,
    pub name: String,
    pub lower: f64,
    pub upper: f64,
    pub image_link: Option<String>,
    pub created_at: Option<NaiveDateTime>,
    pub updated_at: Option<NaiveDateTime>,
    pub game_id: i32,
    pub model_label: String,
}

impl GameModelObject {
    pub fn new(name: String, model_label: String, lower: f64, upper: f64, image_link: Option<String>, game_id: i32) -> Self {
        Self {
            id: 0,
            name,
            model_label,
            lower,
            upper,
            image_link,
            created_at: Some(Utc::now().naive_utc()),
            updated_at: Some(Utc::now().naive_utc()),
            game_id,
        }
    }
}

// TODO: Re-evaluate this later
#[derive(Serialize, Deserialize, Insertable, Debug, Clone)]
#[diesel(table_name = crate::repository::schema::game_rounds)]
pub struct NewGameRoundConfig {
    #[serde(default)]
    pub player_id: Uuid,
    pub game_id: Uuid,
    #[serde(default)]
    pub game_runtime: i32,
    #[serde(default)]
    pub game_accelerator: i32,
    // game data
    #[serde(default)] // allow empty
    pub status: String,
    #[serde(default)]
    pub target: f64,
}

impl NewGameRoundConfig {
    pub fn new(player_id: Uuid, game_id: Uuid, game_runtime: i32, game_accelerator: i32, status: String) -> Self {
        Self {
            player_id,
            game_id,
            game_runtime,
            game_accelerator,
            status,
            target: 0.0, // default
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Selectable, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::game_rounds)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameRoundDAO {
    #[serde(default)]
    pub id: Uuid,
    pub player_id: Uuid,
    pub game_id: Uuid,
    // Game Settings
    pub status: String,
    pub target: f64,
    #[serde(default)]
    pub game_runtime: i32,
    #[serde(default)]
    pub game_accelerator: i32,
    // Game Facts
    #[serde(default)]
    pub start_time: Option<NaiveDateTime>,
    #[serde(default)]
    pub end_time: Option<NaiveDateTime>,
    // Game Metadata
    #[serde(default)]
    pub created_at: NaiveDateTime,
    #[serde(default)]
    pub updated_at: NaiveDateTime,
}

impl GameRoundDAO {
    pub fn from(target: f64, config: NewGameRoundConfig) -> Self {
        Self {
            id: Default::default(),
            player_id: config.player_id,
            game_id: config.game_id,
            status: "REGISTERED".to_string(), // TODO: Make real enums
            target, // TODO: Generate a target score
            game_runtime: config.game_runtime,
            game_accelerator: config.game_accelerator,
            start_time: Some(Utc::now().naive_utc()),
            end_time: None,
            created_at: Default::default(),
            updated_at: Default::default(),
        }
    }
}