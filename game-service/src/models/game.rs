use chrono::NaiveDateTime;
use diesel::prelude::*;
use diesel::Queryable;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::models::game_model::GameModelDAO;

#[derive(Serialize, Deserialize, Debug, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::games)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameDAO {
    #[serde(default)]
    pub id: Uuid,
    #[serde(default)]
    pub name: String,
    #[serde(default)]
    pub description: Option<String>,
    #[serde(default)]
    pub event_name: Option<String>,
    #[serde(default)]
    pub active: bool,
    #[serde(default)]
    pub model_id: Option<i32>,
    pub game_status: String,
    #[serde(default)]
    pub created_at: NaiveDateTime,
    #[serde(default)]
    pub updated_at: NaiveDateTime,
    #[serde(default)]
    pub locale: String,
    #[serde(default)]
    pub round_length: i32,
    #[serde(default)]
    pub min_target: f64,
    #[serde(default)]
    pub max_target: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Game {
    pub id: Uuid,
    pub name: String,
    pub description: Option<String>,
    pub event_name: Option<String>,
    pub active: bool,
    pub model_id: GameModelDAO,
    pub game_status: GameStatus,
    pub created_at: NaiveDateTime,
    pub updated_at: NaiveDateTime,
    pub min_target: f64,
    pub max_target: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::game_status)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameStatus {
    pub status: String,
    pub description: Option<String>,
}

#[derive(Serialize, Deserialize, QueryableByName, Debug, Clone, Queryable, Insertable, AsChangeset)]
#[diesel(table_name = crate::repository::schema::game_scores)]
#[diesel(check_for_backend(diesel::pg::Pg))]
pub struct GameScoreRecord {
    #[serde(default)]
    pub id: i32,
    pub game_round_id: Uuid, // Required
    pub score: f64, // required (even on an update)
    #[serde(default)]
    pub target: f64,
    #[serde(default)]
    pub initials: Option<String>,
    #[serde(default)]
    pub created_at: NaiveDateTime,
}
