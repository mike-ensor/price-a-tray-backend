# Overview

This is the core service that manages Games, Game Instances and running the game

## Database Architecture

### Terms

> Game Model
  * The definition of an ML (CNN) model used by a game. The contained data is used to perform various uses of the model 
    as well as inform the system and users of it's primary functionality.

> Game
  * A game is defined by the overall collection of `GameModel`, `GameRound`, `ModelAccelerator`, `GameStatus`, `GameScoreRecord` and `GameModelObject`

> GameModelObject
  * A single item (physical) used by a `GameModel`. This is the physical piece the player puts on the tray. The data 
    contains the pricing information (non-currency), description, label, image (optional), lower/upper price range for 
    dynamic pricing (future), and the Game it's associated with.

> GameRound
  * Defines a single playing round (typically 30s)

> GameScoreRecord
  * Records the result of a single `GameRound`


## Running Locally

1. Create a `.envrc` file for the Games API container

   ```shell
   export DB_NAME="price_a_tray_games"
   
   export DB_USER="admin_user"
   
   # Local Hosted
   export DB_HOST="localhost"
   export DB_PASS="super_pass"
   
   # K8s Hosted
   #export DB_HOST="192.168.300.300" # not a real IP, put in your IP here
   #export DB_PASS="<some-password>"
   
   # Primary Interface for DB
   export DATABASE_URL="postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}:5432/${DB_NAME}"
   
   export RUN_MODE="development"
   export RUST_LOG="info,actix_web=debug,actix_server=debug,diesel=debug,diesel_logger=info"
   ```

1. Reset DB fully (from project base)
    ```shell
    docker-compose down --volumes
    ```

1. Setup / Run database (from project base)
    ```shell
    docker-compose up -d
    ```

1. Run setup on database
    ```shell
    cd game-service
    diesel setup --database-url=$DATABASE_URL
    ```

1. Run migrations
    ```shell
    diesel migration run
    ```

```shell
cargo run
```

### Watching and rebuilding on change
First, install cargo-watch

```shell
cargo install cargo-watch
```

Then run the application using the following command

```shell
cargo watch -x run
```


### Testing

1. Install Java 17+

1. Download and setup `ijhttp` 

1. Run the `run-api-tests.sh` from the root while the container is running

<!-- https://github.com/dirien/quick-bites/tree/main/rust-actix-web-rest-api-diesel -->