#!/bin/bash
VERSION="$1"
APP="${2}"
DOCKERFILE="${3:-Dockerfile}"

function display_common() {
  echo "Typical Containers:"
  echo "  * game-service"
  echo "  * init-migrations"
}

function build_container() {
  docker build -f "${DOCKERFILE}" -t "${APP}:${VERSION}" --build-arg=INIT_VERSION=${INIT_VERSION} --build-arg=VERSION=${VERSION} .
  if [[ $? -ne 0 ]]; then
      echo "Cannot build docker"
      exit 1
  fi
}

function release_container() {
  docker tag "${APP}:${VERSION}" "${IMAGE_URL}:${VERSION}"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Tag Docker Build with version"
      exit 1
  fi
  docker tag "${APP}:${VERSION}" "${IMAGE_URL}:latest"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Tag Docker Build with latest"
      exit 1
  fi
  docker push "${IMAGE_URL}:${VERSION}"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Push Version Tagged"
      exit 1
  fi
  docker push "${IMAGE_URL}:latest"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Push Latest Tagged"
      exit 1
  fi
}

function create_registry_url() {
  export IMAGE_URL="registry.gitlab.com/${GITLAB_PROJECT}/${APP}"
}

######################## Main

# Check if USE_CLOUD is set to anything, if not, ask if it should be
if [[ -z "${USE_CLOUD}" ]]; then
  echo ""
  read -p "Build using gcloud? If No, build and push locally (y/N): " proceed
  if [[ "${proceed}" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    USE_CLOUD=true
  else
    USE_CLOUD=false
  fi
fi

if [[ -z "${APP}" ]]; then
  echo "Usage: ./build.sh <VERSION> <container-name> <dockerfile-optional>"
  exit 1
fi

REQUIRED_VARIABLES=( "APP" "VERSION" "GITLAB_PROJECT" )
# shellcheck disable=SC2043
HAS_ERROR=0
for REQ in "${REQUIRED_VARIABLES[@]}"
do
  if [[ -z "${REQ}" ]]; then
    echo "Variable ${REQ} is required and does not have a value"
    HAS_ERROR=1
  fi
done

if [[ ${HAS_ERROR} -gt 0 ]]; then
  echo "One or more required variables are not set. Please set and re-run."
  exit 1
fi

if [[ ${USE_CLOUD} == true ]]; then
  # gcloud builds submit --config=cloudbuild.yaml --substitutions=_REPO_REGION="${REPO_REGION}",_REPO_FOLDER="${REPO_FOLDER}",_CONTAINER_NAME="${CONTAINER_NAME}" .
  echo "CANNOT BUILD VIA CLOUD YET"
else
  display_common
  create_registry_url
  echo "Building ${IMAGE_URL}:${VERSION}..."
  build_container
  release_container
fi