#!/bin/bash

function get_files_with_suffix() {
  local directory="$1"
  local suffix="$2"

  # Check if the directory exists
  if [[ ! -d "$directory" ]]; then
    echo "Error: Directory not found: $directory"
    return 1
  fi

  # Use find to get full paths of files with the suffix, suppressing errors (e.g., for broken links)
  find "$directory" -type f -name "$suffix" -print0 2>/dev/null | xargs -0 echo
}

SERVICE="all"
VERBOSITY="${VERBOSITY:-BASIC}"

if [[ ! -z "$1" ]]; then
  SERVICE=$1
fi

TEST_LOCAL=true  # hard-code to true until remote is a thing

if [[ "${TEST_LOCAL}" == true ]]; then
  echo "Testing Local Collections"
  # shellcheck disable=SC2207
  GAME_SERVICES=( $(get_files_with_suffix "$(pwd)/game-service/api-tests" "*.http") )

  if [[ "${SERVICE}" == "all" ]] || [[ "${SERVICE}" == "game" ]]; then
    if [[ ${#GAME_SERVICES[@]} -gt 0 ]]; then
      echo "Running Game Services HTTP Client tests"
      for test in "${GAME_SERVICES[@]}" ; do
        ijhttp -L "${VERBOSITY}" --env-file game-service/api-tests/http-client.private.env.json --env local ${test}
      done
    fi
  fi


fi