# Overview

So you want to develop for this application. This application uses Postgresql (maybe SQLite in future) to store data 
related to the different services. For simplicity (not proper full separation) each service will use the same DB server, 
but will not share their tables.

Exposing services is done using gRPC with the Tonic library

## Environment Setup

> NOTE: This setup assumes you have Docker and docker-compose setup on your machine ahead of time.

1. Install `direnv` and setup a `.envrc` file at the root of this project

   ```bash
   export DB_USER="game_user"     # Default
   export DB_PASS="password123"   # Default
   export DB_NAME="price_a_tray"  # Default
   export DB_HOST="localhost"     # Default

   # this is for development only
   export DATABASE_URL=postgres://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_NAME}
   ```

1. Install the Postgres & SQLite dev libraries
    ```shell
    sudo apt-get install libpq-dev libmysqlclient-dev sqlite3 -y
    ```

1. Run Compose (or provide a postgresql at a URL)

   ```shell
   docker-compose up -d
   ```
   
   | Property          | Default DEV Value | ENV variable |
   |-------------------|-------------------|--------------|
   | POSTGRES_USER     | pg_user           | DB_USER      |
   | POSTGRES_PASSWORD | password123       | DB_PASS      |
   | Database          | price_a_tray      | DB_NAME      |

   > NOTE: Removing data `docker-compose down --volumes` (optional `--rmi all` to remove all images too)

1. Install the diesel CLI tool for interaction and scheme migration assistance

    ```shell
    cargo install diesel_cli --no-default-features --features "postgres sqlite mysql"
    ```
   
2. On a regular basis, run the Migration update

   ```shell
   diesel migration run
   
   # If you want to force re-run
   diesel migration redo
   ```

## One-time Setup

The following section are one-time setup for the project and do not need to be performed if the project is in a built 
and running state. Each will have a "if this exists, then don't do the command(s)"

```shell
diesel setup --database-url="$DATABASE_URL" # this has already been done in the project
diesel migration generate create_base_tables # this has already been done in the project

```
   
## TODO - Future
* gRPC services instead of REST (https://github.com/mpiorowski/sgsg?tab=readme-ov-file or https://github.com/mpiorowski/rusve)
